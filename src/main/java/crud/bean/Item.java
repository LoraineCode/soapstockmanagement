package crud.bean;

import crud.ws.loraine.items.StatusEnum;

import javax.persistence.*;

@Entity
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private int itemCode;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    private float price;

    @ManyToOne
    private Supplier supplier;

    public Item() {
    }

    public Item(int id, String name, int ItemCode, StatusEnum status, int price, Supplier supplier) {
        this.id = id;
        this.name = name;
        this.itemCode = itemCode;
        this.status = status;
        this.price = price;
        this.supplier = supplier;
    }

    public Item(String name, int ItemCode, StatusEnum status, float price, Supplier supplier) {
        this.name = name;
        this.itemCode = itemCode;
        this.status = status;
        this.price = price;
        this.supplier = supplier;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getItemCode() {
        return itemCode;
    }

    public void setItemCode(int ItemCode) {
        this.itemCode = itemCode;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }
}
