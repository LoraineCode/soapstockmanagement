package crud.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebServiceConfig {
    @Bean
    public ServletRegistrationBean messageDispatherServlet(ApplicationContext context){
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(context);
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(messageDispatcherServlet, "/ws/loraine/*");
    }

    @Bean(name = "items")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema itemSchema){
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("ItemPort");
        definition.setTargetNamespace("http://crud/ws/loraine/items");
        definition.setLocationUri("/ws/loraine");
        definition.setSchema(itemSchema);
        return definition;
    }

    @Bean(name = "suppliers")
    public DefaultWsdl11Definition wsdl11Definition(XsdSchema supplierSchema){
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("SupplierPort");
        definition.setTargetNamespace("http://crud/ws/loraine/suppliers");
        definition.setLocationUri("/ws/loraine");
        definition.setSchema(supplierSchema);
        return definition;
    }

    @Bean
    public XsdSchema itemSchema() { return new SimpleXsdSchema(new ClassPathResource("item.xsd"));
    }
    @Bean
    public XsdSchema supplierSchema() { return new SimpleXsdSchema(new ClassPathResource("supplier.xsd"));
    }
}
