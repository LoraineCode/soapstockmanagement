//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.06.25 at 05:41:43 PM EET 
//


package crud.ws.loraine.items;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the crud.ws.loraine.items package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: crud.ws.loraine.items
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UpdateItemRequest }
     * 
     */
    public UpdateItemRequest createUpdateItemRequest() {
        return new UpdateItemRequest();
    }

    /**
     * Create an instance of {@link InsertItemRequest }
     * 
     */
    public InsertItemRequest createInsertItemRequest() {
        return new InsertItemRequest();
    }

    /**
     * Create an instance of {@link GetAllItemResponse }
     * 
     */
    public GetAllItemResponse createGetAllItemResponse() {
        return new GetAllItemResponse();
    }

    /**
     * Create an instance of {@link GetItemRequest }
     * 
     */
    public GetItemRequest createGetItemRequest() {
        return new GetItemRequest();
    }

    /**
     * Create an instance of {@link GetAllItemRequest }
     * 
     */
    public GetAllItemRequest createGetAllItemRequest() {
        return new GetAllItemRequest();
    }

    /**
     * Create an instance of {@link GetItemResponse }
     * 
     */
    public GetItemResponse createGetItemResponse() {
        return new GetItemResponse();
    }

    /**
     * Create an instance of {@link ItemDetails }
     * 
     */
    public ItemDetails createItemDetails() {
        return new ItemDetails();
    }

    /**
     * Create an instance of {@link DeleteItemRequest }
     * 
     */
    public DeleteItemRequest createDeleteItemRequest() {
        return new DeleteItemRequest();
    }

}
