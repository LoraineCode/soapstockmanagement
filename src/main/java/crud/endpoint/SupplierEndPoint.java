package crud.endpoint;

import crud.bean.Supplier;
import crud.repository.ISupplierRepository;
import crud.ws.loraine.suppliers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;

@Endpoint
public class SupplierEndPoint {

    @Autowired private ISupplierRepository supplierRepository;

    @PayloadRoot(namespace = "http://crud/ws/loraine/suppliers", localPart = "GetAllSupplierRequest")
    @ResponsePayload
    public GetAllSupplierResponse getAll(@RequestPayload GetAllSupplierRequest request) {
        GetAllSupplierResponse allSupplierResponse = new GetAllSupplierResponse();
        List<Supplier> supplierList = supplierRepository.findAll();
        for (Supplier supplier : supplierList) {
            GetSupplierResponse supplierResponse = mapSupplierDetails(supplier);
            allSupplierResponse.getSupplierDetails().add(supplierResponse.getSupplierDetails());
        }
        return allSupplierResponse;
    }

    @PayloadRoot(namespace = "http://crud/ws/loraine/suppliers", localPart = "GetSupplierRequest")
    @ResponsePayload
    public GetSupplierResponse getById(@RequestPayload GetSupplierRequest request) {
        Optional<Supplier> supplier = supplierRepository.findById(request.getId());
        if(supplier.isPresent()){
            return mapSupplierDetails(supplier.get());
        }
        return null;
    }

    @PayloadRoot(namespace = "http://crud/ws/loraine/suppliers", localPart = "InsertSupplierRequest")
    @ResponsePayload
    public GetSupplierResponse insertSupplier(@RequestPayload InsertSupplierRequest request) {
        Supplier supplier= insertRequestToSupplier(request.getSupplierDetails());
        supplierRepository.save(supplier);
        return  mapSupplierDetails(supplier);
    }

    @PayloadRoot(namespace = "http://crud/ws/loraine/suppliers", localPart = "UpdateSupplierRequest")
    @ResponsePayload
    public GetSupplierResponse updateSupplier(@RequestPayload UpdateSupplierRequest request) {

        Optional<Supplier> supplier = supplierRepository.findById(mapRequestToSupplier(request.getSupplierDetails()).getId());
        if(supplier.isPresent()){
            supplierRepository.save(mapRequestToSupplier(request.getSupplierDetails()));
            return mapSupplierDetails(supplierRepository.save(mapRequestToSupplier(request.getSupplierDetails())));
        }
        return null;
    }

    @PayloadRoot(namespace = "http://crud/ws/loraine/suppliers", localPart = "DeleteSupplierRequest")
    @ResponsePayload
    public GetSupplierResponse deleteSupplier(@RequestPayload DeleteSupplierRequest request) {
        Optional<Supplier> supplier=supplierRepository.findById(request.getId());
        if(supplier.isPresent()){
            supplierRepository.delete(supplier.get());
            return null;
        }
        return null;
    }

    private GetSupplierResponse mapSupplierDetails(Supplier supplier){
        SupplierDetails supplierDetails = mapSupplier(supplier);

        GetSupplierResponse supplierResponse = new GetSupplierResponse();
        supplierResponse.setSupplierDetails(supplierDetails);

        return supplierResponse;
    }

    private SupplierDetails mapSupplier(Supplier supplier){
        SupplierDetails supplierDetails = new SupplierDetails();
        supplierDetails.setId(supplier.getId());
        supplierDetails.setNames(supplier.getNames());
        supplierDetails.setEmail(supplier.getEmail());
        supplierDetails.setMobile(supplier.getMobile());
        return supplierDetails;
    }
    private Supplier insertRequestToSupplier(SupplierDetails supplierDetails){
        Supplier supplier = new Supplier();
        supplier.setNames(supplierDetails.getNames());
        supplier.setEmail(supplierDetails.getEmail());
        supplier.setMobile(supplierDetails.getMobile());

        return supplier;
    }
    private Supplier mapRequestToSupplier(SupplierDetails supplierDetails){
        Supplier supplier = new Supplier();
        supplier.setId(supplierDetails.getId());
        supplier.setNames(supplierDetails.getNames());
        supplier.setEmail(supplierDetails.getEmail());
        supplier.setMobile(supplierDetails.getMobile());

        return supplier;
    }
}
