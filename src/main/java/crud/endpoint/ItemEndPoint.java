package crud.endpoint;

import crud.bean.Item;
import crud.bean.Supplier;
import crud.repository.ISupplierRepository;
import crud.repository.ItemRepository;
import crud.ws.loraine.items.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;

@Endpoint
public class ItemEndPoint {

    @Autowired private ItemRepository itemRepository;
    @Autowired private ISupplierRepository supplierRepository;

    @PayloadRoot(namespace = "http://crud/ws/loraine/items", localPart = "GetAllItemRequest")
    @ResponsePayload
    public GetAllItemResponse getAll(@RequestPayload GetAllItemRequest request) {
        GetAllItemResponse allItemResponse = new GetAllItemResponse();
        List<Item> itemList = itemRepository.findAll();
        for (Item item : itemList) {
            GetItemResponse itemResponse = mapItemDetails(item);
            allItemResponse.getItemDetails().add(itemResponse.getItemDetails());
        }
        return allItemResponse;
    }

    @PayloadRoot(namespace = "http://crud/ws/loraine/items", localPart = "GetItemRequest")
    @ResponsePayload
    public GetItemResponse getById(@RequestPayload GetItemRequest request) {
        Optional<Item> item = itemRepository.findById(request.getId());
        if(item.isPresent()){
            return mapItemDetails(item.get());
        }
        return null;
    }

    @PayloadRoot(namespace = "http://crud/ws/loraine/items", localPart = "InsertItemRequest")
    @ResponsePayload
    public GetItemResponse insertItem(@RequestPayload InsertItemRequest request) {
        Item item= insertRequestToItem(request.getItemDetails());
        itemRepository.save(item);
        return  mapItemDetails(item);
    }

    @PayloadRoot(namespace = "http://crud/ws/loraine/items", localPart = "UpdateItemRequest")
    @ResponsePayload
    public GetItemResponse updateItem(@RequestPayload UpdateItemRequest request) {

        Optional<Item> item = itemRepository.findById(mapRequestToItem(request.getItemDetails()).getId());
        if(item.isPresent()){
            itemRepository.save(mapRequestToItem(request.getItemDetails()));
            return mapItemDetails(itemRepository.save(mapRequestToItem(request.getItemDetails())));
        }
        return null;
    }

    @PayloadRoot(namespace = "http://crud/ws/loraine/items", localPart = "DeleteItemRequest")
    @ResponsePayload
    public GetItemResponse deleteItem(@RequestPayload DeleteItemRequest request) {
        Optional<Item> item=itemRepository.findById(request.getId());
        if(item.isPresent()){
            itemRepository.delete(item.get());
            return null;
        }
        return null;
    }

    private GetItemResponse mapItemDetails(Item item){
        ItemDetails itemDetails = mapItem(item);

        GetItemResponse itemResponse = new GetItemResponse();
        itemResponse.setItemDetails(itemDetails);

        return itemResponse;
    }

    private ItemDetails mapItem(Item item){
        ItemDetails itemDetails = new ItemDetails();
        itemDetails.setId(item.getId());
        itemDetails.setName(item.getName());
        itemDetails.setItemCode(item.getItemCode());
        itemDetails.setStatus(item.getStatus());
        itemDetails.setPrice(item.getPrice());
        itemDetails.setSupplier(item.getSupplier().getId());
        return itemDetails;
    }
    private Item insertRequestToItem(ItemDetails itemDetails){
        Optional<Supplier> itemSupplier = supplierRepository.findById(itemDetails.getSupplier());
        if(itemSupplier.isPresent()){
            Item item = new Item();
            item.setName(itemDetails.getName());
            item.setItemCode(itemDetails.getItemCode());
            item.setStatus(itemDetails.getStatus());
            item.setPrice(itemDetails.getPrice());
            item.setSupplier(itemSupplier.get());
            return item;
        }
        return null;
    }
    private Item mapRequestToItem(ItemDetails itemDetails){
        Optional<Supplier> itemSupplier = supplierRepository.findById(itemDetails.getSupplier());
        if(itemSupplier.isPresent()){
            Item item = new Item();
            item.setId(itemDetails.getId());
            item.setName(itemDetails.getName());
            item.setItemCode(itemDetails.getItemCode());
            item.setStatus(itemDetails.getStatus());
            item.setPrice(itemDetails.getPrice());
            item.setSupplier(itemSupplier.get());
            return item;
        }
        return null;
    }
}
